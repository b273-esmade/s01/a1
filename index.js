// alert("Hello World!")

// Quiz
/*
1. Using array literals [] or array constructor "new Array()"
2.Using index notation which is [0] or array method like unshift/shift
3. Using index notation which is [array.length -1] or array method like push/pop
4.indexOf()
5.forEach
6.map
7.every
8.some
9.False
10. True
*/

/*Function Coding*/

// 1.
let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students)

function addToEnd(name){
	if (typeof name !== "string"){
		return "Error - Only add strings"
	} else {
		students.push(name)
		return students
	}
}

console.log(addToEnd("Marvs"))



// 2.
function addToStart(name){
	// string ang output ni "typeof"
	if (typeof name !== "string"){
		return "Error - Only add strings"
	} else {
		students.unshift(name)
		return students
	}
}

console.log(addToStart("Evo"))



// 3.
function elementChecker(element){
	if(students.length === 0){
		return "Error - Passed in array is empty"
	}
	else if(students.includes(element)){
		return true
	}
	else {
		return false
	}

}
console.log(elementChecker("Jane")) //true



// 4.
function checkAllStringsEnding(arr, char){
	if (arr.length == 0){
		return "Error - array must not be empty"
	}
	else {
		if (arr.some((elem) => typeof elem !== "string")){
			return "Error - all array elements must be strings"
		}
		else {
			if (typeof char !== "string"){
				return "Error - 2nd argument must be of data type string"
			}
			else {
				if (char.length !== 1){
			    	return "Error - 2nd argument must be a single character";
			  }
			  else {
			  	if (arr.every((elem) => elem.endsWith(char))) {
			  	    return true;
			  	  } else {
			  	    return false;
			  	  }
			  }
			}

		}
	}
}

console.log(checkAllStringsEnding(students, "e")); // false
console.log(checkAllStringsEnding([], "e")); // Error - array must not be empty
console.log(checkAllStringsEnding(["Jane",2], "e")); // Error - all array elements must be strings
console.log(checkAllStringsEnding(students, "el")); //Error - 2nd argument must be a single character
console.log(checkAllStringsEnding(students,4)) //Error - 2nd argument must be of data type string



// 5.
function stringLengthSorter(arr) {
  if (!arr.every(element => typeof element === "string")) {
    return "Error - all elements must be string" ;
  }
  arr.sort((a, b) => a.length - b.length);

  return arr;
}

let sortedStudents = stringLengthSorter(students);
console.log(sortedStudents); //['Evo', 'Joe', 'John', 'Jane', 'Marvs', 'Jessie']



// 6.
function startsWithCounter(arr, char){
	if (arr.length == 0){
		return "Error - array must not be empty"
	}
	else {
		if (arr.some((elem) => typeof elem !== "string")){
			return "Error - all array elements must be strings"
		}
		else {
			if (typeof char !== "string"){
				return "Error - 2nd argument must be of data type string"
			}
			else {
				if (char.length !== 1){
			    	return "Error - 2nd argument must be a single character";
			  }
			  else{ //For Each
			  	let count = 0;
			  	arr.forEach(elem => {
			  		if(elem[0].toLowerCase() === char.toLowerCase()) count++;
			  	})
			  	return count;
			  }


			  // For Loop

			 /* else {
			  	let count = 0;
			  	  	for (let i = 0; i < arr.length; i++) {
			  	    	if (arr[i].charAt(0).toLowerCase() === char.toLowerCase()) {
			  	      		count++;
			  	    	}
			  	  	}
			  	  	return count;
			  }*/



			}
		}
	}
}

console.log(startsWithCounter(students,"j")) //4



 
// 7. 
function likeFinder (arr, char){
		if (arr.length == 0){
			return "Error - array must not be empty"
		}
		else {
			if (arr.some((char)=> typeof char !== "string")){
				return "Error - all array elements must be strings"
			}
			else {
				if (typeof char !== "string"){
					return "Error - 2nd argument must be of data type string"
				}
				else {
					const filteredArr = arr.filter(elem => elem.toLowerCase().includes(char.toLowerCase()));
						return filteredArr;
				}
				
			}
		}
	}


console.log(likeFinder(students,"jo")) //['Joe', 'John']



// 8.
function randomPicker(arr){
		let random = Math.floor(Math.random()*arr.length)
		return arr[random]
	}

console.log(randomPicker(students))
